### configuration file for monitoring

#--------------------------------------------------------------
### navbar configuration

navbar:
  title: scald
  pages:
    - index
    - dash2

  tabs:
    - name: Docs
      url: https://docs.ligo.org/gstlal-visualisation/ligo-scald
    - name: Git
      url: https://git.ligo.org/gstlal-visualisation/ligo-scald


#--------------------------------------------------------------
### page configuration

pages:
  index:
    title: Example 1
    type: grid
    mode: online
    lookback: 300
    delay: 0
    plots:
      - plot: snr_by_ifo
        grid: {x: 0, y: 0, h: 6, w: 6}
        visible: true
      - plot: heartbeat
        grid: {x: 0, y: 6, h: 6, w: 6}
        visible: true
      - plot: segment
        grid: {x: 6, y: 0, h: 6, w: 6}
        visible: true
      - plot: snr_heatmap
        grid: {x: 6, y: 6, h: 6, w: 6}
        visible: true

  dash2:
    title: Example 2
    type: grid
    mode: online
    lookback: 300
    delay: 0
    plots:
      - plot: binned
        grid: {x: 0, y: 0, h: 6, w: 6}
        visible: true
      - plot: multischema
        grid: {x: 0, y: 6, h: 6, w: 6}
        visible: true
      - plot: custom_opts
        grid: {x: 0, y: 12, h: 6, w: 6}
        visible: true
      - plot: twoaxis
        grid: {x: 6, y: 0, h: 6, w: 6}
        visible: true
      - plot: ifar
        grid: {x: 6, y: 6, h: 12, w: 6}
        visible: true


#--------------------------------------------------------------
### schema configuration

schemas:
  snr:
    measurement: inspiral
    column: snr
    tag: ifo
    tag_key: ifo
    ifo:
      - H1
      - L1
    aggregate: median
    default: 0

  H1_snr:
    measurement: inspiral
    column: snr
    tag: ifo
    tag_key: ifo
    ifo:
      - H1
    aggregate: median

  L1_snr:
    measurement: inspiral
    column: snr
    tag: ifo
    tag_key: ifo
    ifo:
      - L1
    aggregate: median

  H1L1_snr:
    measurement: inspiral
    axis1:
      schema:
        column: snr
        tag: ifo
        tag_key: ifo
        ifo:
          - H1
        aggregate: median
    axis2:
      schema:
        column: snr
        tag: ifo
        tag_key: ifo
        ifo:
          - L1
        aggregate: median

  H1L1V1_snr:
    measurement: inspiral
    schema1:
      name: H1
      column: snr
      tag: ifo
      tag_key: ifo
      ifo:
        - H1
      aggregate: median
    schema2:
      name: L1
      column: snr
      tag: ifo
      tag_key: ifo
      ifo:
        - L1
      aggregate: median
    schema3:
      name: V1
      column: snr
      tag: ifo
      tag_key: ifo
      ifo:
        - V1
      aggregate: median

  snr_by_job:
    measurement: inspiral
    column: snr
    tag: job
    tag_key: job
    aggregate: median

  heartbeat:
    backend: custom
    measurement: latency
    column: latency
    tag: job
    tag_key: job
    aggregate: median
    default: 1e4
    transform: latency

  latency:
    backend: custom
    measurement: latency
    column: latency
    tag: job
    tag_key: job
    aggregate: median
    default: 1e4
    transform: none

  segment:
    segment:
      measurement: segment
      column: seg
      tag: job
      tag_key: job
      aggregate: median
    timeseries:
      measurement: segment
      name: "TimeSeries"
      column: snr
      tag: ifo
      tag_key: ifo
      ifo:
        - "TimeSeries"
      aggregate: median

  ifar:
    measurement: inspiral
    column: far
    far: 1e-3
    lookback: 10000

  binned:
    measurement: inspiral
    tbin: 10
    dt: 10
    column: snr
    tag: ifo
    tag_key: ifo
    ifo:
      - H1
    aggregate: median


#--------------------------------------------------------------
### plot configuration

plots:
  snr_by_ifo:
    title: SNR Timeseries by IFO
    type: TimeSeries
    schema: snr
    layout:
      yaxis:
        type: log
        title: {text: SNR}

  twoaxis:
    title: Two Axis, H1,L1 SNR Timeseries
    type: MultiAxis
    schema: H1L1_snr
    layout:
      yaxis: {title: 'y-axis 1, H1'}
      yaxis2: {title: 'y-axis 2, L1'}

  multischema:
    title: Multi Schema, SNR Timeseries by IFO
    type: MultiSchema
    schema: H1L1V1_snr

  custom_opts:
    title: Customizable DataOptions
    type: TimeSeries
    schema: snr
    layout:
      yaxis:
        type: log
        title: {text: SNR}
      margin: {l: 40, r: 20, t: 10, b: 20}
    data_options:
      H1:
        mode: 'markers'
        color: 'rgb(55, 128, 191)'
      L1:
        mode: 'lines+markers'
        marker:
          color: 'rgb(219, 64, 82)'
          size: 2
        line:
          width: .5
          shape: 'vh'
          dash: 'dashdot'
      colorscale: 'Viridis'

  snr_heatmap:
    title: Sample SNR Heatmap by Job
    type: TimeHeatMap
    schema: snr_by_job
    layout:
      yaxis: {title: {text: Job ID}}

  segment:
    title: TimeSeries + Segment Plot
    type: TimeSegment
    schema: segment
    layout:
      yaxis: {title: {text: 'TimeSeries Axis'}}
    data_options:
      segment:
        colorscale: [['0.0', 'rgb(212,22,22)'],['1.0', 'rgb(48,133,5)']]
      timeseries:
        colorscale: 'Viridis'

  heartbeat:
    title: Latest Heartbeat by Job
    type: Bar
    schema: heartbeat
    layout:
      xaxis: {title: {text: 'Job ID'}}
      yaxis: {title: {text: 'Latency [s]'}}

  ifar:
    title: IFAR Plot + Table
    type: IFAR
    schema: ifar
    layout:
      xaxis:
        title: {text: 'Inverse False-Alarm Rate [s]'}
        type: log
      yaxis:
        title: {text: 'Number of Events'}
        type: log

  binned:
    title: SNR Binned Time Histogram
    type: BinnedTimeHistogram
    schema: binned
    layout:
      yaxis: {title: {text: SNR}}


#--------------------------------------------------------------
### default plot settings

plotly:
  default:
    layout:
      font:
        family: "'Nunito', sans-serif"
        size: 12
        color: '#666'
      plot_bgcolor: 'rgba(0,0,0,0)'
      paper_bgcolor: 'rgba(0,0,0,0)'
      showlegend: 'true'
    data_options:
      marker:
        color: 'rgb(255, 0, 0)'
      colorscale: "Viridis"
      H1:
        mode: 'lines'
        line:
          width: 0.5
          color: 'rgb(238,0,0)'
      L1:
        mode: 'lines'
        line:
          width: 0.5
          color: 'rgb(75,166,255)'
      V1:
        mode: 'lines'
        line:
          width: 0.5
          color: 'rgb(155,89,182)'

  TimeSeries:
    layout:
      margin: {l: 50, r: 0, t: 0, b: 20}

  TimeHeatMap:
    layout:
      margin: {l: 50, r: 0, t: 10, b: 25}

  IFAR:
    layout:
      margin: {l: 60, r: 0, t: 10, b: 50}

  Bar:
    layout:
      margin: {l: 50, r: 0, t: 10, b: 40}


#--------------------------------------------------------------
### nagios configuration

nagios:
  heartbeat:
    lookback: 180
    schema: heartbeat
    alert_type: heartbeat


#--------------------------------------------------------------
### data backend configuration

backends:
  default:
    backend: influxdb
    db: mydb
    hostname: localhost
    port: 8086

  custom:
    backend: influxdb
    db: myotherdb
    hostname: localhost
    port: 8086

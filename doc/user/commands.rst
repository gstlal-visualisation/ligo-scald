==========
Commands
==========

.. contents::
   :local:


**ligo-scald** provides a command line interface for various tasks:

:code:`scald serve`: serving data and dynamic html pages

:code:`scald deploy`: deploys a web application on the LDG

:code:`scald mock`: starts up a mock database that generates data based on HTTP requests

:code:`scald report`: generates offline html reports


:code:`scald serve`
~~~~~~~~~~~~~~~~~~~

This command allow access to a dynamic dashboard and serves data
from a variety of data backends. It does this by running a web server that responds
to HTTP queries.

It is required to provide a configuration file that defines the data products that
can be queried, as well as the plots that get shown on the dashboard. This can be done
by providing a configuration file by :code:`-c /path/to/config.yml` or by setting
:code:`SCALDRC_PATH` to point towards the configuration.

.. program-output:: scald serve --help
   :nostderr:


:code:`scald deploy`
~~~~~~~~~~~~~~~~~~~~

This command sets everything needed to deploy :code:`scald serve` on the LDG, or any place
where running a local web server is not appropriate. This leverages the :code:`public_html/cgi-bin`
folder that is used to run CGI applications by the LDG's web server.

.. program-output:: scald deploy --help
   :nostderr:


:code:`scald mock`
~~~~~~~~~~~~~~~~~~~~

This command starts up a mock database that is used for testing purposes and to run a local dashboard
based on randomly generated data. Specifically, this mocks a subset of a InfluxDB database, and so
instantiating an influx Consumer and making queries will return back data in the same way as a production
database.

.. program-output:: scald mock --help
   :nostderr:


:code:`scald report`
~~~~~~~~~~~~~~~~~~~~

This command generate offline-style HTML reports given a configuration file. This is an alternative to
the programmatic report interface, and can be used for prototyping quick changes to a auto-generated
report configuration file.

.. program-output:: scald report --help
   :nostderr:
